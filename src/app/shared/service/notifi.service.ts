import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable()

export class NotifiService {
    constructor ( public toastr : ToastrService ) {}

    showSuccess(data : string ){

        console.log("this.user info is >>>", data);
        this.toastr.success(" successfully logged in.", data);
    }

    showwarning( data : string){
        this.toastr.warning('Are you sure to log out?',`${data}`);
    }

    showInfo(data : string){
        this.toastr.info('congratulation', `${data}`);
    }

    showError(){
        this.toastr.error('Something went wrong.')
    }
}