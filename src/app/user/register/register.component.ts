import { Component, OnInit } from '@angular/core';
import { User } from './../../models/userModel'
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public user;
  constructor( public router : Router ) { 
    this.user = new User({});
  }

  public submit(){
    this.router.navigate(['/dashboard']);
  }


  ngOnInit() {
  }

}
