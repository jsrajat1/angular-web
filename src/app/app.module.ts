import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'


import { AppComponent } from './app.component';
import { navcomponent } from './navBar/navBar.component';
import { loginComponent } from './login/login.component';
import { RegisterComponent } from './user/register/register.component';
import { ForgotPasswordComponent } from './user/forgot-password/forgot-password.component';
import { appRoutingModule } from './appRouting';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotifiService } from './shared/service/notifi.service';
import { from } from 'rxjs';

@NgModule({
  declarations: [
    AppComponent,
    navcomponent,
    loginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    PageNotFoundComponent,
    DashboardComponent
    
  ],
  imports: [
    // BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    appRoutingModule,
    ToastrModule.forRoot()
  ],
  providers: [NotifiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
