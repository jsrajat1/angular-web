import { Component, OnInit } from '@angular/core';
import { User } from './../models/userModel'
import { Router } from '@angular/router';
import { NotifiService } from '../shared/service/notifi.service';
@Component({
    selector: "app-login",
    templateUrl: "./login.html",
    styleUrls: ["./login.css"]

})
export class loginComponent implements OnInit {
    public user;
    constructor(
        public router: Router,
        public notifyService : NotifiService
        ) {
        this.user = new User({});

    }

    ngOnInit() {

    }
    clicked() {
        console.log("user info is >>>", JSON.stringify(this.user));

        this.notifyService.showSuccess(this.user);
        setTimeout(() => {
           this.router.navigate(['/dashboard']);
        }, 3000);
    }
}